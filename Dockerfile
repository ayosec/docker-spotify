FROM debian:jessie

ENV HOME /spotify
ENV DISPLAY :0

RUN useradd --create-home --home-dir $HOME spotify

ADD spotify.list /etc/apt/sources.list.d/spotify.list
ADD asoundrc /spotify/.asoundrc

RUN ln /bin/true /usr/bin/xdg-icon-resource
RUN ln /bin/true /usr/bin/xdg-desktop-menu

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886 && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
      spotify-client libpangoxft-1.0-0 x11vnc xvfb libasound2-plugins mdbus2 ratpoison dbus-x11

ADD cmds/ /spotify/

USER spotify
ENTRYPOINT [ "/spotify/system" ]
